# lineargraph

## Description
A LaTeX/TikZ package for drawing linear graph schematics.
A linear graph is made of nodes and oriented arcs that carry lumped
physical components.

The package provides commands for drawing:
- nodes, with label
- reference nodes, with label
- two-terminal components, with label and flow annotation
- four-terminal components, with label and two flow annotations

## Requirements
- LaTeX
- TikZ
- xstring

## Installation
Copy the package file _lineargraph.sty_ into a LaTeX search directory like `/usr/share/texmf`.

In the preambule of a document, add:
> \usepackage{lineargraph}

## Contents
- lineargraph.sty, the package file
- lineargraph-doc.pdf, the package documentation
- lineargraph-doc.tex, the source code for package documentation
- examples/, a folder with some examples of linear graph schematics

## Getting started
Compile with pdflatex the following document:

> \documentclass{standalone}  
> \usepackage{lineargraph}  
> \begin{document}  
> \begin{tikzpicture}  
>    \refnode{1.5,0}{north}{$e_0$}  
>    \twotermBTzn{0,0}{SE}{$f_0$}  
>    \anode{0,3}{north west}{$e_1$}  
>    \twotermLRnn{0,3}{R}{$f_1$}  
>    \anode{3,3}{north east}{$e_2$}  
>    \twotermTBnz{3,3}{C}{$f_2$}  
>    \draw (0,0) -- (1.4,0) (1.6,0) -- (3,0);  
> \end{tikzpicture}  
> \end{document}  

This yields the following network schematics:

![schematics](examples/getstarted.png)
