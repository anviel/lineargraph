% -----------------------------------------------------------------------------
% File      : lineargraph-doc.tex
% Object    : documentation linear graph schematic
% Author    : A. Viel
% Created on: 2024-MAR-26
% -----------------------------------------------------------------------------
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{units}
\usepackage{xstring}
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage{lineargraph}

\addtolength{\textwidth}{3cm}
\addtolength{\oddsidemargin}{-1.5cm}
\setlength{\parindent}{0mm}

\begin{document}
\newenvironment{minip}[1]{\begin{minipage}{#1}\begin{center}}{\end{center}\end{minipage}}

\title{Documentation of the \emph{lineargraph} package\\Version 0.1}
\date{2024-MAR-26}
\author{A. Viel}
\maketitle

\tableofcontents

% -----------------------------------------------------------------------------
\section{Description, installation \& use}
% -----------------------------------------------------------------------------
\subsection{Description}
This \LaTeX{}/Tikz package provides commands for drawing linear graphs \cite{Trent}, also known as
Kirchhoff networks representing lumped physical systems under the assumptions
of flow conservation (according to Kirchhoff Node Law) and energy conservation (according to Tellegen theorem). The building bricks of a linear graph are
oriented arcs that carry a physical element or component. A flow quantity is associated with the arcs and is circulating through the components. At the two ends of the arcs there are \emph{terminals}, which enable to connect components together. When at least two terminals are connected a new \emph{node} entity is created. An effort quantity is associated with each node. Commands are provided for drawing multiple terminal components (for the moment only 2 or 4 terminals) and nodes, including a reference or "grounded" node.
\subsection{Installation \& use}
Requirements:
\begin{itemize}
\item \emph{latex}
\item \emph{tikz}
\item \emph{xstring}
\end{itemize}
Copy the package file \texttt{lineargraph.sty} into a \LaTeX{} search directory, like \texttt{/usr/share/texmf}.

Then, use it:
\begin{verbatim}
\usepackage{lineargraph}
\end{verbatim}

This package defines new commands to be executed in a \emph{tikzpicture} environment only.

% -----------------------------------------------------------------------------
\section{Two terminal components}
% -----------------------------------------------------------------------------
\subsection{Common definitions}
This family of commands draw an arc carrying a component. A black triangle arrow gives the orientation of the flow.
\begin{verbatim}
   \twotermBTzz{<coordinates>}{<component label>}{<flow label>}
\end{verbatim}
The first argument are the coordinates of the form \texttt{x,y} of the
starting point of the arc. The second argument is the component label which
is written in the component box. The third argument is the label for the flow,
which is written near the flow arrow.

Each component has four versions labeled \texttt{zz}, \texttt{nz}, \texttt{zn}, and \texttt{nn}. It specifies the
offset at both ends of the arc carrying the component. If this is \emph{z} (zero), no offset is applied. If this is \emph{n} (node), an offset of \unit[0.1]{cm} is applied, to give enough room to draw a node.

% -----------------------------------------------------------------------------
\subsection{Vertical two terminal components}
% -----------------------------------------------------------------------------
The commands called \verb+\twotermBT..+ draw an arc from the bottom to the top, the starting point (first argument) being at the bottom.
The commands called \verb+\twotermTB..+ draw an arc from the top to the bottom, the starting point (first argument) being at the top.

On figure (\ref{fig:vttc}), from left to right:
\begin{verbatim}
   \twotermBTzz{0,0}{zz}{f}
   \twotermBTnz{2,0}{nz}{f}
   \twotermBTzn{4,0}{zn}{f}
   \twotermBTnn{6,0}{nn}{f}
\end{verbatim}
\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
   \draw [gray,dotted] (0,0) grid (6,3);
   \twotermBTzz{0,0}{zz}{f}
   \twotermBTnz{2,0}{nz}{f}
   \twotermBTzn{4,0}{zn}{f}
   \twotermBTnn{6,0}{nn}{f}
\end{tikzpicture}
\end{center}
\caption{Vertical two-terminal components}
\label{fig:vttc}
\end{figure}

On figure (\ref{fig:vttc2}), from left to right:
\begin{verbatim}
   \twotermTBzz{0,3}{zz}{f}
   \twotermTBnz{2,3}{nz}{f}
   \twotermTBzn{4,3}{zn}{f}
   \twotermTBnn{6,3}{nn}{f}
\end{verbatim}
\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
   \draw [gray,dotted] (0,0) grid (6,3);
   \twotermTBzz{0,3}{zz}{f}
   \twotermTBnz{2,3}{nz}{f}
   \twotermTBzn{4,3}{zn}{f}
   \twotermTBnn{6,3}{nn}{f}
\end{tikzpicture}
\end{center}
\caption{Vertical two-terminal components}
\label{fig:vttc2}
\end{figure}

% -----------------------------------------------------------------------------
\subsection{Horizontal two terminal components}
% -----------------------------------------------------------------------------
The commands called \verb+\twotermLR..+ draw an arc from the left to the right, the starting point (first argument) being at the left.
The commands called \verb+\twotermRL..+ draw an arc from the right to the left, the starting point (first argument) being at the right.

On figure (\ref{fig:httc}), from top to bottom:
\begin{verbatim}
   \twotermLRnn{0,6}{nn}{f}
   \twotermLRzn{0,4}{zn}{f}
   \twotermLRnz{0,2}{nz}{f}
   \twotermLRzz{0,0}{zz}{f}
\end{verbatim}
\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
   \draw [gray,dotted] (0,0) grid (3,6);
   \twotermLRzz{0,0}{zz}{f}
   \twotermLRnz{0,2}{nz}{f}
   \twotermLRzn{0,4}{zn}{f}
   \twotermLRnn{0,6}{nn}{f}
\end{tikzpicture}
\end{center}
\caption{Horizontal two-terminal components}
\label{fig:httc}
\end{figure}

On figure (\ref{fig:httc2}), from top to bottom:
\begin{verbatim}
   \twotermRLnn{3,6}{nn}{f}
   \twotermRLzn{3,4}{zn}{f}
   \twotermRLnz{3,2}{nz}{f}
   \twotermRLzz{3,0}{zz}{f}
\end{verbatim}
\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
   \draw [gray,dotted] (0,0) grid (3,6);
   \twotermRLzz{3,0}{zz}{f}
   \twotermRLnz{3,2}{nz}{f}
   \twotermRLzn{3,4}{zn}{f}
   \twotermRLnn{3,6}{nn}{f}
\end{tikzpicture}
\end{center}
\caption{Horizontal two-terminal components}
\label{fig:httc2}
\end{figure}

% -----------------------------------------------------------------------------
\section{Four terminal components}
% -----------------------------------------------------------------------------
A four terminal component (also known as coupler, transformer or gyrator)
is a component that is carried by two arcs. The arc on the left side of the component is the primary, and the arc on the right side is the secondary. There is a flow along the arc at the primary side, and another distinct flow along the arc at the secondary side. Black triangle arrows are drawn to specify the
orientation of the flow along the primary and secondary arcs.
\begin{verbatim}
   \fourterm....{<coordinates>}{<component label>}
                {<primary flow label>}{<secondary flow label>}
\end{verbatim}
The four dots are replaced by \texttt{z} or \texttt{n}, to specify the offset
to be applied at the ends of the arc: \unit[0]{cm} for \emph{z} (zero), or
\unit[0.1]{cm} for \emph{n} (node), to give room to draw a node.
The offsets are specified in the following order:
\begin{enumerate}
   \item at top left (primary side)
   \item at bottom left (primary side)
   \item at top right (secondary side)
   \item at bottom right (secondary side)
\end{enumerate}

On figure (\ref{fig:ftc}):
\begin{verbatim}
   \fourtermzznz{0,0}{zznz}{fp}{fs}
   \fourtermnzzn{5,0}{nzzn}{fp}{fs}
\end{verbatim}
\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
   \draw [gray,dotted] (0,0) grid (9,2);
   \fourtermzznz{0,0}{zznz}{fp}{fs}
   \fourtermnzzn{5,0}{nzzn}{fp}{fs}
\end{tikzpicture}
\end{center}
\caption{Four terminal components}
\label{fig:ftc}
\end{figure}

% -----------------------------------------------------------------------------
\section{Nodes}
% -----------------------------------------------------------------------------
An ordinary node can be created using this command:
\begin{verbatim}
   \anode{<coordinates>}{<label position>}{<node label>}
\end{verbatim}
The first argument are the coordinates of the form \texttt{x,y} of the
the node. The second argument specifies the position of the label with respect to the node. Allowed values are:
\begin{itemize}
\item north
\item north east
\item east
\item south east
\item south
\item south west
\item west
\item north west 
\end{itemize}
The third argument is the label for the node, usually the nodal effort.

A reference node is created with the command:
\begin{verbatim}
   \refnode{<coordinates>}{<label position>}{<node label>}
\end{verbatim}
The arguments are the same as for the \verb+\anode+ command except the
label position that can only take the following values:
\begin{itemize}
\item east
\item west
\item north
\item north east
\item north west 
\end{itemize}

On figure (\ref{fig:nd}):
\begin{verbatim}
   \anode{0,1}{west}{n}
   \refnode{1,1}{north}{r}
\end{verbatim}
\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
   \draw [gray,dotted] (0,0) grid (2,2);
   \anode{0,1}{west}{n}
   \refnode{1,1}{north}{r}
\end{tikzpicture}
\end{center}
\caption{Nodes}
\label{fig:nd}
\end{figure}

% -----------------------------------------------------------------------------
\section{Network examples}
% -----------------------------------------------------------------------------
Figure (\ref{fig:netw}) shows the linear graph for a simple network made
of 5 two-terminal components, 3 ordinary nodes and a reference node.
\begin{verbatim}
   \twotermBTzn{0,0}{SE}{$f_0$}
   \anode{0,3}{west}{$e_1$}
   \twotermLRnn{0,3}{R$_1$}{$f_1$}
   \anode{3,3}{north}{$e_2$}
   \twotermTBnn{3,3}{C$_1$}{$f_2$}
   \twotermLRnn{3,3}{R$_2$}{$f_3$}
   \anode{6,3}{east}{$e_3$}
   \twotermTBnz{6,3}{C$_2$}{$f_4$}
   \refnode{3,0}{north west}{$e_0$}
   \draw (0,0) -- (2.9,0) (3.1,0) -- (6,0); % closing lines
\end{verbatim}
\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
   \twotermBTzn{0,0}{SE}{$f_0$}
   \anode{0,3}{west}{$e_1$}
   \twotermLRnn{0,3}{R$_1$}{$f_1$}
   \anode{3,3}{north}{$e_2$}
   \twotermTBnn{3,3}{C$_1$}{$f_2$}
   \twotermLRnn{3,3}{R$_2$}{$f_3$}
   \anode{6,3}{east}{$e_3$}
   \twotermTBnz{6,3}{C$_2$}{$f_4$}
   \refnode{3,0}{north west}{$e_0$}
   % closing lines
   \draw (0,0) -- (2.9,0) (3.1,0) -- (6,0);
\end{tikzpicture}
\end{center}
\caption{Example of a simple network}
\label{fig:netw}
\end{figure}

Figure (\ref{fig:netw4}) shows the linear graph for a network made of
a four-terminal component at the center, and 2 two-terminal components at
the primary side, and 2 other two-terminal components at the secondary side.
Each side define 2 ordinary nodes and one reference node.

The primary side is first defined, with its reference node labeled $e_0$:
\begin{verbatim}
   \refnode{1.5,0}{north}{$e_0$}
   \twotermBTzn{0,0}{SE}{$f_0$}
   \anode{0,3}{west}{$e_1$}
   \twotermLRnn{0,3}{R$_1$}{$f_1$}
   \anode{3,3}{north}{$e_p$}
   % closing lines, primary
   \draw (0,0) -- (1.4,0) (1.6,0) -- (3,0) -- (3,1) (3,2) -- (3,2.9);
\end{verbatim}

Then, a transformer as a four-terminal component:
\begin{verbatim}
   \fourtermzzzz{3,0.5}{TF}{$f_p$}{$f_s$}
\end{verbatim}

Finally, the secondary side is drawn, with another reference node labeled $e^\prime_0$:
\begin{verbatim}
   \anode{7,3}{north}{$e_s$}
   \twotermLRnn{7,3}{R$_2$}{$f_2$}
   \anode{10,3}{east}{$e_2$}
   \twotermTBnz{10,3}{C}{$f_3$}
   \refnode{8.5,0}{north}{$e^\prime_0$}
   % closing lines, secondary
   \draw (7,2) -- (7,2.9);
   \draw (7,1) -- (7,0) -- (8.4,0) (8.6,0) -- (10,0);
\end{verbatim}
\begin{figure}[htbp]
\begin{center}
\begin{tikzpicture}
   % primary side
   \refnode{1.5,0}{north}{$e_0$}
   \twotermBTzn{0,0}{SE}{$f_0$}
   \anode{0,3}{west}{$e_1$}
   \twotermLRnn{0,3}{R$_1$}{$f_1$}
   \anode{3,3}{north}{$e_p$}
   % closing lines, primary
   \draw (0,0) -- (1.4,0) (1.6,0) -- (3,0) -- (3,1) (3,2) -- (3,2.9);
   % transformer
   \fourtermzzzz{3,0.5}{TF}{$f_p$}{$f_s$}
   % secondary side
   \anode{7,3}{north}{$e_s$}
   \twotermLRnn{7,3}{R$_2$}{$f_2$}
   \anode{10,3}{east}{$e_2$}
   \twotermTBnz{10,3}{C}{$f_3$}
   \refnode{8.5,0}{north}{$e^\prime_0$}
   % closing lines, secondary
   \draw (7,2) -- (7,2.9);
   \draw (7,1) -- (7,0) -- (8.4,0) (8.6,0) -- (10,0);
\end{tikzpicture}
\end{center}
\caption{Example of a network with a four terminal component}
\label{fig:netw4}
\end{figure}

\section{References}
\begin{thebibliography}{9}
\bibitem{Trent} Trent H. M., \emph{Isomorphisms between Oriented Linear Graphs and Lumped Physical Systems}, J. Acoust. Soc. Am., Vol. 27, No 3, May 1955.
\end{thebibliography}

\end{document}
